#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

    NetworkFeatureStability
        
    A library for calculating the stability of topological features in
    functional networks.
    
    Note: this file includes examples on how the library should be used.
    For details on specific functions, please refer to the comments in
    NetworkFeatureStability.py
    

    If you use this library, please acknowledge:
    
        M. Zanin et al. (2022) Telling functional networks apart using ranked network features stability. In publication.
    
"""

import numpy as np

import NetworkFeatureStability as nfs



"""
Let us start by creating a set of time series, representing the dynamics
of a set of nodes - e.g. the time series recorded by an EEG machine for
a single subject.
For the sake of simplicity, we consider 10 time series (i.e. 10 nodes in
the functional network), each one composed of 1000 values. Also, the
time series are completely random.
"""
numNodes = 10
numValues = 1000
timeSeries = np.random.normal( 0.0, 1.0, ( numNodes, numValues ) )



"""
Next, we have to set some variables, defining the parameters associated to
the analysis.

We start with numPossibleLocations, i.e. the number of elements which can have
the maximim value of the topological metric. For instance, in the case of node
centrality, this number will be equal to the number of nodes. On the other
hand, in the case of e.g. link centrality, this should be equal to the number
of possible links in the network.

We also set the window sizes for which we want the analysis to be executed.
"""
numPossibleLocations = numNodes
windowSizes = [ 10, 20, 40, 80 ]



"""
We then define the method that will be used to reconstruct the functional
networks, starting from the provided time series. This should include both
a reference to the function itself, and to a set of parameters if necessary
(e.g. the threshold for binarising the network).

Note that one example is included in NetworkFeatureStability.py
Yet, any other custom function can be used!
"""
networkCreationMethod = nfs.NetworkCreationMethods_AbsLinearCorrelation_Threshold
networkCreationArguments = 0.5



"""
Next comes the definition of the function that calculates which element
of the network has the maximum value of a given topological metric. Again,
this should include both a reference to the function itself, and to a set of 
parameters if so required.

To illustrate, the function here used calculates the degree centrality of
all nodes, and then returns the index of the node with the largest centrality.
Also note that, in this specific case, no arguments are required.
"""
featureRankingMethod = nfs.FeatureRanking_NodeDegreeCentrality
featureRankingArguments = ''



"""
At last, the main function of the library is called, with all previous
parameters. The result is a Numpy array, including the window size and the
probability to it associated - see the main paper for a theoretical
discussion on this approach.
"""
result = nfs.CalculateFeatureStability( timeSeries, numPossibleLocations = numPossibleLocations, \
                                        windowSizes = windowSizes, networkCreationMethod = networkCreationMethod, \
                                        networkCreationArguments = networkCreationArguments, \
                                        featureRankingMethod = featureRankingMethod, featureRankingArguments = featureRankingArguments )

    



"""
For the sake of completeness, this is another example, in which we calculate
the stability of the node with larger clustering coefficient.
"""
networkCreationMethod = nfs.NetworkCreationMethods_AbsLinearCorrelation_Threshold
networkCreationArguments = 0.5

featureRankingMethod = nfs.FeatureRanking_NodeClustering
featureRankingArguments = ''

result = nfs.CalculateFeatureStability( timeSeries, numPossibleLocations = numPossibleLocations, \
                                        windowSizes = windowSizes, networkCreationMethod = networkCreationMethod, \
                                        networkCreationArguments = networkCreationArguments, \
                                        featureRankingMethod = featureRankingMethod, featureRankingArguments = featureRankingArguments )

    



"""
Final example: we locally create a function that returns a node at random
(yes, not really a great topological metric, but it is the simplest
example), and call the main function with it.
"""
def SillyRankingFeature( AM, arguments ):
    numNodes = np.size( AM, axis = 0 )
    return np.random.randint( numNodes )

featureRankingMethod = SillyRankingFeature
featureRankingArguments = ''

result = nfs.CalculateFeatureStability( timeSeries, numPossibleLocations = numPossibleLocations, \
                                        windowSizes = windowSizes, networkCreationMethod = networkCreationMethod, \
                                        networkCreationArguments = networkCreationArguments, \
                                        featureRankingMethod = featureRankingMethod, featureRankingArguments = featureRankingArguments )
