# Network Feature Stability

Companion software for the paper:

Massimiliano Zanin, Bahar Güntekin, Tuba Aktürk, Ebru Yıldırım, Görsev Yener, Ilayda Kiyi, Duygu Hünerli-Gündüz, Henrique Sequeira
Telling functional networks apart using ranked network features stability



## Getting started

Check the file Usage_example.py for examples on how to use the library. Information about parameters and other relevant issues can be found in the paper.

Note that the implementation has been developed in-house, and as such may contain errors or inefficient code; we welcome readers to send us comments, suggestions and corrections, using the "Issues" feature.



## Change log

**2021.11.09** First public version of the library.

