#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

    NetworkFeatureStability
        
    A library for calculating the stability of topological features in
    functional networks.
    
    Functions:
    
        CalculateFeatureStability
            Calculate the stability of a topological feature (defined through
            the call to a method), starting from a set of time series from
            which functional networks are reconstructed. See the comments
            in the function itself for further details.
            
            
        NetworkCreationMethods_AbsLinearCorrelation_Threshold
            An example of a method to reconstruct a functional network given
            a set of time series. Specifically, a link between two nodes is 
            created whenever the absolute value of the linear correlation
            between the two corresponding time series surpasses a fixes
            threshold.
            
            
        FeatureRanking_NodeDegreeCentrality
        FeatureRanking_NodeClustering
            Two examples of a function calculating the ranking according to
            a topological feature. In all cases, the input must be the adjacency
            matrix (with or without weights), and the output the index of the
            element with the largest value of the topological feature -
            e.g. the index of the node with the largest degree centrality.


    If you use this library, please acknowledge:
    
        M. Zanin et al. (2022) Telling functional networks apart using ranked network features stability. In publication.
    
"""

import numpy as np
from scipy.stats import binom




def CalculateFeatureStability( timeSeries, numPossibleLocations, windowSizes, \
                               networkCreationMethod, networkCreationArguments, \
                               featureRankingMethod, featureRankingArguments ):
    """
    Main function of this library, calculates the stability of a topological
    feature, given a set of time series from which the functional network is
    reconstructed, and a window length.
    Check the main paper for details on the approach.

    Parameters
    ----------
    timeSeries : Numpy array, ( number of nodes x length )
        Set of time series from which the network is reconstructed
        
    numPossibleLocations : int
        Number of network elements on which the topological metric is calculated.
        For instance, in the case of node centrality, this will be the number
        of nodes.
        
    windowSizes : list
        List with possible window lengths, used to reconstruct sub-networks.
        
    networkCreationMethod : function
        Method to be used to reconstruct the functional networks. See below
        for an example.
        
    networkCreationArguments : any
        Parameter(s) to be passed to the previous method.
        
    featureRankingMethod : function
        Method to be used for calculating the element of the network with the
        largest topological feature. See below for an example.
        
    featureRankingArguments : any
        Parameter(s) to be passed to the previous method.

    Returns
    -------
    Numpy array
        Pairs of window length and corresponding probability.

    """
    
    probabilityEvolution = []
    

    for windowSize in windowSizes:
    
        freqTopologicalMetric = np.zeros( ( numPossibleLocations ) )
        
        numWindows = int( np.size( timeSeries, 1 ) / windowSize )        
        
        for k in range( numWindows ):
            subTS = timeSeries[:, (k*windowSize) : ((k+1)*windowSize)]
            
            network = networkCreationMethod( subTS, networkCreationArguments )
            topLocation = featureRankingMethod( network, featureRankingArguments )
            
            freqTopologicalMetric[ topLocation ] += 1
    

        newProb = binom.logpmf( np.max( freqTopologicalMetric ), \
                      np.sum( freqTopologicalMetric ), 1 / numPossibleLocations )
        probabilityEvolution.append( ( windowSize, newProb ) )


    return np.array( probabilityEvolution )






def NetworkCreationMethods_AbsLinearCorrelation_Threshold( timeSeries, arguments ):
    """
    Example of a function for reconstructing functional networks. It simply
    calculates the absolute value of the linear correlation between all pairs
    of nodes; for then applying a thresholding (defined in the arguments
    parameter).

    Parameters
    ----------
    timeSeries : Numpy array
        Set of time series used to reconstruct the network.
    arguments : float
        Threshold used to prune the functional network.

    Returns
    -------
    AM : Numpy matrix
        Obtained adjacency matrix.

    """
    
    WM = np.abs( np.corrcoef( timeSeries ) )
    AM = np.array( WM >= arguments, dtype = int )
    
    numNodes = np.size( AM, 0 )
    for n in range( numNodes ):
        AM[ n, n ] = 0
    
    return AM




def FeatureRanking_NodeDegreeCentrality( AM, arguments ):
    """
    Example of a function ranking nodes according to their centrality.

    Parameters
    ----------
    AM : Numpy matrix
        Adjacency matrix of the functional network.
    arguments : None
        Not used here.

    Returns
    -------
    nodeOfMaxDegree : int
        Offset of the node with the highest degree centrality.

    """
    
    degrees = np.sum( AM, axis = 0 )
    nodeOfMaxDegree = np.argmax( degrees )
    
    return nodeOfMaxDegree




def FeatureRanking_NodeClustering( AM, arguments ):
    """
    Example of a function ranking nodes according to their clustering coefficient.

    Parameters
    ----------
    AM : Numpy matrix
        Adjacency matrix of the functional network.
    arguments : None
        Not used here.

    Returns
    -------
    nodeOfMaxDegree : int
        Offset of the node with the highest clustering coefficient.

    """
    
    import networkx as nx
    
    numNodes = np.size( AM, axis = 0 )
    Graph = nx.from_numpy_matrix( AM )
    clusteringValue = np.zeros( ( numNodes ) )
    
    for n in range( numNodes ):
        clusteringValue[ n ] = nx.clustering( Graph, n, weight = 'weight' )

    nodeOfMaxClustering = np.argmax( clusteringValue )
    
    return nodeOfMaxClustering

